FROM golang:1.12.9

RUN sed -i s@/deb.debian.org/@/mirrors.aliyun.com/@g /etc/apt/sources.list && \
    sed -i s@/security.debian.org/@/mirrors.aliyun.com/@g /etc/apt/sources.list && \
    apt-get update && apt-get install -y --no-install-recommends \
                openssh-client \
                rsync \
                fuse \
                sshfs && \
    rm -rf /var/lib/apt/lists/*

RUN export GO111MODULE=on && \
    export GOPROXY=https://goproxy.cn && \
    go get golang.org/x/lint/golint \
       github.com/mattn/goveralls \
       golang.org/x/tools/cover

ENV USER root
WORKDIR /go/src/github.com/docker/machine

COPY . ./
RUN mkdir bin
